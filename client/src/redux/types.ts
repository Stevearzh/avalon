import { Dispatch as ReduxDispatch } from 'redux';

import { RootState } from './';

export type Dispatch = ReduxDispatch<RootState>;
